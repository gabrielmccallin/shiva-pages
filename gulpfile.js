var gulp = require('gulp');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var tsify = require('tsify');
var sourcemaps = require('gulp-sourcemaps');
var buffer = require('vinyl-buffer');
var connect = require('gulp-connect');
var watchify = require("watchify");
var gutil = require("gulp-util");
var uglify = require("gulp-uglify");
var ts = require('gulp-typescript');

var SOURCE = "src";
var TARGET = "serve";


gulp.task('webserver', function () {
    connect.server({
        livereload: {
            port: 13337
        },
        port: 1337,
        root: "serve",
        fallback: __dirname + '/serve/index.html'
    });
});

var watchedBrowserify = watchify(browserify({
    basedir: 'src',
    debug: true,
    entries: ['App.ts'],
    cache: {},
    packageCache: {}
}).plugin(tsify));

function watch() {
    return watchedBrowserify
        .bundle()
        .pipe(source('bundle.js'))
        .pipe(buffer())
        .pipe(sourcemaps.init({ loadMaps: true })) // loads map from browserify file
        // .pipe(uglify({ mangle: false }))
        .pipe(sourcemaps.write("/"
            ,
            {
                sourceRoot: "/src"
            }))
        .pipe(gulp.dest("serve"))
        .pipe(connect.reload());
}

gulp.task('publish', function () {
    var b = browserify({
        entries: ['src/App.ts'],
    }).plugin(tsify);

    return b.bundle()
        .pipe(source('bundle.js'))
        .pipe(buffer())
        .pipe(uglify())
        .on('error', gutil.log)
        .pipe(gulp.dest('./serve'));
});


gulp.task("default", ['webserver'], watch);
watchedBrowserify.on("update", watch);
watchedBrowserify.on("log", gutil.log);

(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
(function (global){
!function(t){function e(){}function n(t,e){return function(){t.apply(e,arguments)}}function i(t){if("object"!=typeof this)throw new TypeError("Promises must be constructed via new");if("function"!=typeof t)throw new TypeError("not a function");this._state=0,this._handled=!1,this._value=void 0,this._deferreds=[],l(t,this)}function o(t,e){for(;3===t._state;)t=t._value;return 0===t._state?void t._deferreds.push(e):(t._handled=!0,void i._immediateFn(function(){var n=1===t._state?e.onFulfilled:e.onRejected;if(null===n)return void(1===t._state?r:s)(e.promise,t._value);var i;try{i=n(t._value)}catch(o){return void s(e.promise,o)}r(e.promise,i)}))}function r(t,e){try{if(e===t)throw new TypeError("A promise cannot be resolved with itself.");if(e&&("object"==typeof e||"function"==typeof e)){var o=e.then;if(e instanceof i)return t._state=3,t._value=e,void a(t);if("function"==typeof o)return void l(n(o,e),t)}t._state=1,t._value=e,a(t)}catch(r){s(t,r)}}function s(t,e){t._state=2,t._value=e,a(t)}function a(t){2===t._state&&0===t._deferreds.length&&i._immediateFn(function(){t._handled||i._unhandledRejectionFn(t._value)});for(var e=0,n=t._deferreds.length;e<n;e++)o(t,t._deferreds[e]);t._deferreds=null}function c(t,e,n){this.onFulfilled="function"==typeof t?t:null,this.onRejected="function"==typeof e?e:null,this.promise=n}function l(t,e){var n=!1;try{t(function(t){n||(n=!0,r(e,t))},function(t){n||(n=!0,s(e,t))})}catch(i){if(n)return;n=!0,s(e,i)}}var u=setTimeout;i.prototype["catch"]=function(t){return this.then(null,t)},i.prototype.then=function(t,n){var i=new this.constructor(e);return o(this,new c(t,n,i)),i},i.all=function(t){var e=Array.prototype.slice.call(t);return new i(function(t,n){function i(r,s){try{if(s&&("object"==typeof s||"function"==typeof s)){var a=s.then;if("function"==typeof a)return void a.call(s,function(t){i(r,t)},n)}e[r]=s,0===--o&&t(e)}catch(c){n(c)}}if(0===e.length)return t([]);for(var o=e.length,r=0;r<e.length;r++)i(r,e[r])})},i.resolve=function(t){return t&&"object"==typeof t&&t.constructor===i?t:new i(function(e){e(t)})},i.reject=function(t){return new i(function(e,n){n(t)})},i.race=function(t){return new i(function(e,n){for(var i=0,o=t.length;i<o;i++)t[i].then(e,n)})},i._immediateFn="function"==typeof setImmediate&&function(t){setImmediate(t)}||function(t){u(t,0)},i._unhandledRejectionFn=function(t){"undefined"!=typeof console&&console&&console.warn("Possible Unhandled Promise Rejection:",t)},i._setImmediateFn=function(t){i._immediateFn=t},i._setUnhandledRejectionFn=function(t){i._unhandledRejectionFn=t},"undefined"!=typeof module&&module.exports?module.exports=i:t.Promise||(t.Promise=i)}(this),function(){var t,e=this&&this.__extends||function(t,e){function n(){this.constructor=t}for(var i in e)e.hasOwnProperty(i)&&(t[i]=e[i]);t.prototype=null===e?Object.create(e):(n.prototype=e.prototype,new n)};!function(t){var e=function(){function t(){}return t.style=function(t,e){var n;n=t.element?t.element:t;for(var i in e)if(e.hasOwnProperty(i)){var o=e[i];if("number"==typeof o&&o)switch(i){case"x":o=e[i].toString(),o+="px";break;case"y":o=e[i].toString(),o+="px";break;case"height":o=e[i].toString(),o+="px";break;case"width":o=e[i].toString(),o+="px";break;case"backgroundColor":o="rgb("+(e[i]>>16)+","+(e[i]>>8&255)+","+(255&e[i])+")";break;case"color":o="rgb("+(e[i]>>16)+","+(e[i]>>8&255)+","+(255&e[i])+")"}var r=i;switch(i){case"x":r="left";break;case"y":r="top";break;case"alpha":r="opacity"}n.style[r]=o}},t}();t.Properties=e}(t||(t={}));var t;!function(t){var e=function(){function t(t,e,n,i){this._type=t,this._target=e,this._sourceEvent=i,this._data=n}return Object.defineProperty(t.prototype,"target",{get:function(){return this._target},enumerable:!0,configurable:!0}),Object.defineProperty(t.prototype,"type",{get:function(){return this._type},enumerable:!0,configurable:!0}),Object.defineProperty(t.prototype,"data",{get:function(){return this._data},set:function(t){this._data=t},enumerable:!0,configurable:!0}),Object.defineProperty(t.prototype,"sourceEvent",{get:function(){return this._sourceEvent},enumerable:!0,configurable:!0}),t}();t.Event=e;var n=function(){function t(){this._listeners=[]}return t.prototype.hasEventListener=function(t,e){for(var n=!1,i=0;i<this._listeners.length;i++)this._listeners[i].type===t&&this._listeners[i].listener===e&&(n=!0);return n},t.prototype.addEventListener=function(t,e,n,i,o,r){void 0===o&&(o=!1),void 0===r&&(r=void 0),this.hasEventListener(e,n)||this._listeners.push({scope:t,type:e,listener:n,useCapture:o,scopedEventListener:r,data:i})},t.prototype.removeEventListener=function(t,e){var n=this._listeners.filter(function(n){return n.type===t&&n.listener.toString()===e.toString()});return this._listeners=this._listeners.filter(function(n){return!(n.type===t&&n.listener.toString()===e.toString())}),n[0]},t.prototype.dispatchEvent=function(t){for(var e=0;e<this._listeners.length;e++)this._listeners[e].type===t.type&&(this._listeners[e].data&&(t.data=this._listeners[e].data),this._listeners[e].listener.call(this._listeners[e].scope,t))},t}();t.EventDispatcher=n}(t||(t={}));var t;!function(t){var n=function(n){function i(t){n.call(this),this.transitions={},t?(t.root?(this._element=document.createElement("div"),this._element.style.position="absolute",this._element.style.height="100%",this._element.style.width="100%",this._element.style.top="0px",this._element.style.left="0px",this._element.style.margin="0px",this._element.id="app",document.body.appendChild(this._element)):t.type?this._element=document.createElement(t.type):this._element=document.createElement("div"),t.id&&(this._element.id=t.id),t.text&&(this.innerHtml=t.text),this.style(t.style),this.style(t)):this._element=document.createElement("div")}return e(i,n),i.prototype.addToBody=function(){document.body.appendChild(this._element)},i.prototype.style=function(e){t.Properties.style(this._element,e)},i.prototype.className=function(){for(var t=this,e=[],n=0;n<arguments.length;n++)e[n-0]=arguments[n];e.map(function(e){t._element.className=e})},i.prototype.addChild=function(t){var e;t.element&&(e=t.element),this._element.appendChild(e)},i.prototype.removeChild=function(t){this._element===t.element.parentNode&&this._element.removeChild(t.element)},i.prototype.to=function(e){var n=this,i=10;return e.delay&&(i=1e3*e.delay),setTimeout(function(){for(var t in e.toVars){var i={};e.duration&&(i.duration=e.duration),n.transitions[t]?i.count=n.transitions[t].count+1:i.count=0,n.transitions[t]=i}n.style({transition:n.convertTransitionObjectToString(n.transitions)}),e.ease&&n.style({transitionTimingFunction:e.ease.toString()}),n.style(e.toVars)},i),e.resolve?(setTimeout(function(){n.style({transition:n.removeCompletedTransitionsAndReapply(e.toVars)}),n.dispatchEvent(new t.Event("TRANSITION_COMPLETE",n)),e.resolve()},1e3*e.duration+i),null):new Promise(function(o,r){setTimeout(function(){n.style({transition:n.removeCompletedTransitionsAndReapply(e.toVars)}),o(),n.dispatchEvent(new t.Event("TRANSITION_COMPLETE",n))},1e3*e.duration+i)})},i.prototype.convertTransitionStyleToObject=function(t){var e=this;if(t.transitionProperty){var n=t.transitionProperty.split(","),i=t.transitionDuration.split(","),o=t.transitionDelay.split(","),r={},s=0;return n.map(function(t){var n=t.replace(/^\s/g,"");n=e.hyphenToCamel(n);var a={};i[s].replace(/^\s/g,"");if(i[s]&&(a.duration=i[s].replace(/^\s/g,"").replace("s","")),o[s]){var c=o[s].replace(/^\s/g,"").replace("s","");"initial"!==c&&(a.delay=c)}r[n]=a,s++}),r}return{}},i.prototype.convertTransitionObjectToString=function(t){var e="";for(var n in t){""!==e&&(e+=", ");var i=this.camelToHyphen(n);e+=i+" "+t[n].duration+"s",t[n].delay&&(e+=" "+t[n].delay+"s")}return e},i.prototype.removeCompletedTransitionsAndReapply=function(t){for(var e in t)this.transitions[e]&&(this.transitions[e].count>0?this.transitions[e].count--:delete this.transitions[e]);return this.convertTransitionObjectToString(this.transitions)},i.prototype.fromTo=function(t){var e=this;return t.delay?(t.delay=1e3*t.delay,t.immediateRender&&this.style(t.fromVars)):(this.style(t.fromVars),t.delay=10),new Promise(function(n,i){setTimeout(function(){e.style(t.fromVars),setTimeout(function(){e.to({duration:t.duration,ease:t.ease,toVars:t.toVars,resolve:n})},10)},t.delay)})},i.prototype.camelToHyphen=function(t){return t.replace(/[a-z][A-Z]/g,function(t,e){var n=t.split("");n[2]=n[1],n[1]="-",n[2]=n[2].toLowerCase();var i="";return n.map(function(t){i+=t}),i})},i.prototype.hyphenToCamel=function(t){return t.replace(/-([a-z])/g,function(t,e){return t[1].toUpperCase()})},i.prototype.addEventListener=function(e,i,o,r,s){void 0===s&&(s=!1);var a=this,c=function(n){o.apply(e,[new t.Event(i,a,r,n)])};n.prototype.addEventListener.call(this,e,i,o,r,s,c),this._element.addEventListener?this._element.addEventListener(i,c,s):this._element.attachEvent&&(this._element.attachEvent("on"+i,c),this._element.attachEvent("onpropertychange",function(t){t.eventType===i&&(t.cancelBubble=!0,t.returnValue=!1,t.data=t.customData,c(t))}))},i.prototype.removeEventListener=function(t,e){var i=n.prototype.removeEventListener.call(this,t,e);return this._element.removeEventListener?this._element.removeEventListener(t,i.scopedEventListener,i.useCapture):this._element.detachEvent&&this._element.detachEvent("on"+t,e),i},i.prototype.preventDefault=function(t){t.preventDefault?t.preventDefault():t.returnValue=!1},Object.defineProperty(i.prototype,"width",{get:function(){return this.shadow().width},set:function(e){t.Properties.style(this._element,{width:e})},enumerable:!0,configurable:!0}),Object.defineProperty(i.prototype,"height",{get:function(){return this.shadow().height},set:function(e){t.Properties.style(this._element,{height:e})},enumerable:!0,configurable:!0}),Object.defineProperty(i.prototype,"y",{get:function(){return this._element.offsetTop},set:function(e){t.Properties.style(this._element,{y:e})},enumerable:!0,configurable:!0}),Object.defineProperty(i.prototype,"x",{get:function(){return this._element.offsetLeft},set:function(e){t.Properties.style(this._element,{x:e})},enumerable:!0,configurable:!0}),Object.defineProperty(i.prototype,"alpha",{get:function(){return parseFloat(this._element.style.opacity)},set:function(e){t.Properties.style(this._element,{opacity:e.toString()})},enumerable:!0,configurable:!0}),i.prototype.hide=function(){t.Properties.style(this._element,{display:"none"})},i.prototype.show=function(){t.Properties.style(this._element,{display:"block"})},i.prototype.fillContainer=function(){t.Properties.style(this._element,{minWidth:"100%",minHeight:"100%",left:"50%",top:"50%",transform:"translate(-50%, -50%)",position:"relative"})},i.prototype.centreHorizontal=function(){t.Properties.style(this._element,{display:"block",marginLeft:"auto",marginRight:"auto",position:"relative"})},i.prototype.centreHorizontalText=function(){t.Properties.style(this._element,{textAlign:"center"})},i.prototype.shadow=function(){if(document.body.contains(this._element))return this.dimensionsPolyfill();var t=this._element.parentElement;document.body.appendChild(this._element);var e=this.dimensionsPolyfill();return document.body.removeChild(this._element),t?t.appendChild(this._element):document.body.removeChild(this._element),e},i.prototype.dimensionsPolyfill=function(){var e=this._element.getBoundingClientRect().height,n=this._element.getBoundingClientRect().width;n&&e||(n=this._element.scrollWidth,e=this._element.scrollHeight);var i=new t.Dimensions(n,e);return i},Object.defineProperty(i.prototype,"value",{get:function(){var t=this._element;return t.value},set:function(t){var e=this._element;e.value=t},enumerable:!0,configurable:!0}),Object.defineProperty(i.prototype,"id",{get:function(){return this._element.id},set:function(t){this._element.id=t},enumerable:!0,configurable:!0}),Object.defineProperty(i.prototype,"element",{get:function(){return this._element},enumerable:!0,configurable:!0}),Object.defineProperty(i.prototype,"innerHtml",{get:function(){return this._element.innerHTML},set:function(t){this._element.innerHTML=t},enumerable:!0,configurable:!0}),Object.defineProperty(i.prototype,"href",{get:function(){var t=this._element;return t.href},set:function(t){var e=this._element;e.href=t},enumerable:!0,configurable:!0}),i.TRANSITION_COMPLETE="TRANSITION_COMPLETE",i}(t.EventDispatcher);t.Container=n}(t||(t={}));var t;!function(t){var n=function(t){function n(e){e.type="a",t.call(this,e);var n=this.element;n.href=e.href}return e(n,t),n}(t.Container);t.Anchor=n}(t||(t={}));var t;!function(t){var n=function(n){function i(e){var i;i=e.href?"a":"button",e.type&&(i=e.type),n.call(this,{id:e.id,type:i,cursor:"pointer",display:"inline-block"}),this.href=e.href,this.enabled=!0,e.icon&&e.icon.code&&(this.icon=new t.Container({type:"span",text:e.icon.code})),this.update(e)}return e(i,n),i.prototype.update=function(e){this.config={};for(var n in t.Styles.button)this.config[n]=t.Styles.button[n];if(e&&e.style)for(var n in e.style)this.config[n]=e.style[n];for(var n in e)this.config[n]=e[n];if(e.style)for(var n in e.style.icon)this.config.icon[n]=e.style.icon[n];this.innerHtml=e.text,this.addEventListener(this,"mouseover",this.overWithEnable),this.addEventListener(this,"mouseout",this.outWithEnable),this.config.icon&&this.config.icon.code&&("left"===this.config.icon.align?this.icon.style({paddingRight:this.config.icon.padding}):this.icon.style({paddingLeft:this.config.icon.padding}),this.icon.style({fontFamily:this.config.icon.fontFamily,fontSize:this.config.icon.fontSize,"float":this.config.icon.align,pointerEvents:"none"}),this.addChild(this.icon)),this.style(this.config)},i.prototype.over=function(){this.to({duration:this.config.durationIn,toVars:{backgroundColor:this.config.backgroundColorHover,color:this.config.colorHover}})},i.prototype.out=function(){this.to({duration:this.config.durationOut,toVars:{backgroundColor:this.config.backgroundColor,color:this.config.color}})},i.prototype.click=function(e){if(this.enabled){var n=new t.Event(i.CLICK,this,e);this.dispatchEvent(n)}},i.prototype.disable=function(){this.enabled=!1,this.style({cursor:"default"})},i.prototype.select=function(){this.enabled=!1,this.style({cursor:"default"})},i.prototype.enable=function(){this.enabled=!0,this.style({cursor:"pointer"}),this.out()},Object.defineProperty(i.prototype,"data",{get:function(){return this.config.data},enumerable:!0,configurable:!0}),i.prototype.overWithEnable=function(t){this.enabled&&this.over()},i.prototype.outWithEnable=function(t){this.enabled&&this.out()},i.CLICK="click",i}(t.Container);t.Button=n}(t||(t={}));var t;!function(t){var n=function(t){function n(e){t.call(this,{type:"input"});var n=this.element;n.type="checkbox",e&&(e.id&&(this.id=e.id),this.style(e.style),this.style(e),n.checked=e.checked)}return e(n,t),Object.defineProperty(n.prototype,"checked",{get:function(){var t=this.element;return t.checked},enumerable:!0,configurable:!0}),n.CLICK="click",n}(t.Container);t.CheckBox=n}(t||(t={}));var t;!function(t){var e=function(){function t(t,e){this.width=t,this.height=e}return t}();t.Dimensions=e}(t||(t={}));var t;!function(t){var e=function(){function t(){}return t.Linear="linear",t.Ease="ease",t.EaseIn="ease-in",t.EaseOut="ease-out",t.EaseInOut="ease-in-out",t}();t.Ease=e}(t||(t={}));var t;!function(t){var n=function(t){function n(e){var n;n=e.style?e.style:{},n.type="img",t.call(this,n),this.load(e.path)}return e(n,t),n.prototype.load=function(t){this.element.setAttribute("src",t)},n.COMPLETE="load",n.ERROR="error",n}(t.Container);t.Image=n}(t||(t={}));var t;!function(t){var n=function(n){function i(){n.call(this)}return e(i,n),i.prototype.load=function(e,n,i,o,r){var s=this;return this.http?this.http.abort():this.http=new XMLHttpRequest,n===t.Loader.GET&&(e+=this.concatParams(i)),this.http.open(n,e,!0),this.http.timeout=2e4,o&&o.map(function(t){s.http.setRequestHeader(t.value,t.variable)}),this.http.onreadystatechange=this.handleResponse.bind(this),new Promise(function(t,e){s.resolve=t,s.reject=e,s.http.send(i)})},i.prototype.concatParams=function(t){var e="?";for(var n in t)t.hasOwnProperty(n)&&(e=e.concat(n,"=",encodeURI(t[n]),"&"));return e=e.slice(0,-1)},i.prototype.setRequestHeader=function(t){this.http.setRequestHeader(t.value,t.variable)},i.prototype.handleResponse=function(){if(4===this.http.readyState)if(200===this.http.status){var e=new t.LoaderEvent(i.COMPLETE,this,this.http.responseText,this.http.status,this.http);n.prototype.dispatchEvent.call(this,e),this.resolve(this.http.responseText),this.http.onreadystatechange=void 0}else{var o=void 0;o=0===this.http.status?"Network Error 0x2ee7":this.http.statusText;var r=new t.LoaderEvent(i.ERROR,this,o,this.http.status,this.http);n.prototype.dispatchEvent.call(this,r),this.reject({error:o,status:this.http.status})}},i.COMPLETE="COMPLETE",i.ERROR="ERROR",i.GET="GET",i.PUT="PUT",i.POST="POST",i.UPDATE="UPDATE",i}(t.EventDispatcher);t.Loader=n}(t||(t={}));var t;!function(t){var n=function(t){function n(e,n,i,o,r,s,a){t.call(this,e,n,s,a),this._response=i,this._status=o,this._httpMetaData=r}return e(n,t),Object.defineProperty(n.prototype,"response",{get:function(){return this._response},enumerable:!0,configurable:!0}),Object.defineProperty(n.prototype,"status",{get:function(){return this._status},enumerable:!0,configurable:!0}),Object.defineProperty(n.prototype,"httpMetaData",{get:function(){return this._httpMetaData},enumerable:!0,configurable:!0}),n}(t.Event);t.LoaderEvent=n}(t||(t={}));var t;!function(t){var e=function(){function t(){}return t.addEventListener=function(t,e,n){this.observers[e]||(this.observers[e]=[]),this.observers[e].push({scope:t,type:e,callback:n})},t.removeEventListener=function(t,e){for(var n,i=0;i<this.observers[t].length;i++)if(this.observers[t].callback===e){n=i;break}this.observers[t].splice(n,1)},t.dispatchEvent=function(t){var e=t.type;if(this.observers[e])for(var n=0;n<this.observers[e].length;n++)this.observers[e][n].callback.call(this.observers[e][n].scope,t);else console.log("DISPATCH ERROR: NO OBSERVER REGISTERED")},t.observers={},t}();t.Observer=e}(t||(t={}));var t;!function(t){var n=function(t){function n(e){t.call(this,{id:e.id,position:"relative"}),this.pages={},this.zIndex=100,this.config=e,this.style(e.style)}return e(n,t),n.prototype.update=function(t){var e=this;if(t!==this.currentPageName){if(this.config.pages[t]){if(clearTimeout(this.delayTimeout),this.currentPageName=t,this.currentPage)if(this.currentPage.sleep&&this.currentPage.sleep(),this.config.delayTransition){var n=this.currentPage;this.delayTimeout=setTimeout(function(){e.removeChild(n)},1e3*this.config.delayTransition)}else this.removeChild(this.currentPage);if(this.pages[t]);else{var i=new this.config.pages[t](t);this.pages[t]=i}this.currentPage=this.pages[t],this.currentPage.wake&&this.currentPage.wake(),this.currentPage.style({position:"absolute",width:"100%",top:"0px",left:"0px"}),this.addChild(this.currentPage)}}else console.log("view already loaded: ")},n}(t.Container);t.Pages=n}(t||(t={}));var t;!function(t){var n=function(t){function n(e){t.call(this,{type:"input"});var n=this.element;n.type="radio",e&&(e.id&&(this.id=e.id),this.style(e.style),this.style(e),n.checked=e.checked)}return e(n,t),Object.defineProperty(n.prototype,"checked",{get:function(){var t=this.element;return t.checked},enumerable:!0,configurable:!0}),n.CLICK="click",n}(t.Container);t.RadioButton=n}(t||(t={}));var t;!function(t){var e=function(){function t(){}return t.proportionalOutside=function(t,e,n,i){var o=t/e,r=n,s=n/o;return s<i&&(s=i,r=s*o),{height:s,width:r}},t.proportionalInside=function(t,e,n,i){var o=t/e,r=n,s=n*o;return s>i&&(s=i,r=s*o),{height:s,width:r}},t}();t.Resize=e}(t||(t={}));var t;!function(t){var n=function(n){function i(e){var i=this;e.type="select",n.call(this,e);var o=this.element;e.name&&(o.name=e.name);var r=e.options;r.map(function(e){var n=new t.Container({text:e,type:"option"});i.addChild(n)})}return e(i,n),Object.defineProperty(i.prototype,"value",{get:function(){var t=this.element;return t.value},enumerable:!0,configurable:!0}),Object.defineProperty(i.prototype,"selectedIndex",{get:function(){var t=this.element;return t.selectedIndex},enumerable:!0,configurable:!0}),i.CHANGE="change",i}(t.Container);t.Select=n}(t||(t={}));var t;!function(t){var e=function(){function t(){}return t.button={fontSize:"1.2em",fontFamily:"sans-serif",fontWeight:"300",fontColourOver:3355443,fontColourOut:16777215,letterSpacing:"0em",backgroundColor:"#f1f1f1",backgroundColorHover:"#dddddd",cornerRadius:"0.5em",durationOut:1,durationIn:0,padding:"0.5em",textAlign:"left",whiteSpace:"nowrap",msTouchAction:"manipulation",touchAction:"manipulation",cursor:"pointer",webkitUserSelect:"none",mozUserSelect:"none",msUserSelect:"none",userSelect:"none",backgroundImage:"none",border:"1px solid transparent",borderColor:"#dddddd",color:"#333333",colorHover:"#ffffff",text:"button"},t.caret={width:"0px",height:"0px",borderLeftWidth:"0.35rem",borderLeftStyle:"solid",borderLeftColor:"transparent",borderRightWidth:"0.35rem",borderRightStyle:"solid",borderRightColor:"transparent",borderTopWidth:"0.35rem",borderTopStyle:"solid",borderTopColor:"black",display:"inline-block",verticalAlign:"middle",marginLeft:"0.35rem"},t.drop={fontFamily:"sans-serif",fontSize:"1.2rem",backgroundColor:"#ffffff",backgroundColorHover:"#cccccc",colorHover:"#000000",color:"#000000",durationIn:0,durationOut:.5,listStyle:"none",zIndex:"1000",position:"absolute",marginTop:"0px",minWidth:"5rem",border:"1px solid rgba(0,0,0,.15)",webkitBoxShadow:"0 6px 12px rgba(0,0,0,.175)",boxShadow:"0px 6px 12px rgba(0,0,0,0.175)",fontWeight:"300",paddingLeft:"0px",durationExpand:"0.5",durationContract:"0.5"},t}();t.Styles=e}(t||(t={}));var t;!function(t){var e=function(){function t(){this.callback=function(){console.log("not set")}}return t.prototype.then=function(e,n){return this.callback=e,console.log("callback: ",this.callback),new t},t.prototype.execute=function(){this.callback&&this.callback(this.data)},t.prototype.printCallback=function(){return this},t}();t.Transition=e}(t||(t={}));var t;!function(t){var e=function(){function t(){}return t.scrollY=function(){var t=document.body.scrollTop;return 0==t&&(t=window.pageYOffset?window.pageYOffset:document.body.parentElement?document.body.parentElement.scrollTop:0),t},t.scrollX=function(){var t=document.body.scrollLeft;return 0==t&&(t=window.pageXOffset?window.pageXOffset:document.body.parentElement?document.body.parentElement.scrollLeft:0),t},Object.defineProperty(t,"height",{get:function(){return window.innerHeight||document.documentElement.clientHeight||document.body.clientHeight},enumerable:!0,configurable:!0}),Object.defineProperty(t,"width",{get:function(){return window.innerWidth||document.documentElement.clientWidth||document.body.clientWidth},enumerable:!0,configurable:!0}),t}();t.Window=e}(t||(t={}));var t;!function(t){var n=function(n){function i(e){var i=this,o="Button";e.text&&(o=e.text),e.text=void 0,e.position="relative",n.call(this,e),this.items=[],this.button=new t.Button({style:t.Styles.button,display:"inline-block",text:"button",type:"div",zIndex:"1001",position:"absolute"}),e.button&&(e.button.text=o,this.button.update(e.button)),this.addChild(this.button);var r=new t.Container({id:"drop-caret",style:t.Styles.caret,pointerEvents:"none"});e.caret&&r.style(e.caret),this.button.addChild(r),this.button.addEventListener(this,"mousedown",this.buttonClicked),this.unorderedList=new t.Container({type:"ul",id:this.id}),this.dropConfig={};for(var s in t.Styles.drop)this.dropConfig[s]=t.Styles.drop[s];for(var s in e.drop)this.dropConfig[s]=e.drop[s];this.unorderedList.style(this.dropConfig),this.addChild(this.unorderedList);var a=0;e.options.map(function(e){var n=new t.Container({id:a.toString(),type:"li"});i.unorderedList.addChild(n);var o=new t.Container({id:a.toString(),type:"a"});o.style({display:"list-item",position:"static",color:i.dropConfig.color,paddingLeft:"0.5em",paddingRight:"0px",paddingBottom:"0.5em",paddingTop:"0.5em",cursor:"pointer"}),i.items.push(n),o.innerHtml=e,o.addEventListener(i,"mouseover",i.itemOver),o.addEventListener(i,"mouseout",i.itemOut),o.addEventListener(i,"mousedown",i.itemClicked),n.addChild(o),a++}),this.unorderedList.style({display:"none"}),this.style(e)}return e(i,n),i.prototype.itemClicked=function(e){var n=e.target;this.dispatchEvent(new t.Event(i.CHANGE,this,n.id)),this.unorderedList.style({opacity:"1"}),this.unorderedList.addEventListener(this,t.Container.TRANSITION_COMPLETE,this.hideList),this.unorderedList.to({duration:this.dropConfig.durationContract,delay:.3,toVars:{opacity:"0"}})},i.prototype.itemOver=function(t){var e=t.target;e.to({duration:this.dropConfig.durationIn,toVars:{backgroundColor:this.dropConfig.backgroundColorHover,color:this.dropConfig.colorHover}})},i.prototype.itemOut=function(t){var e=t.target;e.to({duration:this.dropConfig.durationOut,toVars:{backgroundColor:this.dropConfig.backgroundColor,color:this.dropConfig.color}})},i.prototype.buttonClicked=function(t){var e=this;this.unorderedList.style({display:"block",opacity:"0",top:"0px"}),this.unorderedList.to({duration:this.dropConfig.durationExpand,toVars:{alpha:1,y:this.button.height}}),this.scopedEventHandler=function(t){e.closeDrop(t)},document.body.addEventListener("mousedown",this.scopedEventHandler,!0),this.button.removeEventListener("mousedown",this.buttonClicked)},i.prototype.closeDrop=function(e){var n=this;document.body.removeEventListener("mousedown",this.scopedEventHandler,!0),setTimeout(function(){n.button.addEventListener(n,"mousedown",n.buttonClicked)},10),this.unorderedList.addEventListener(this,t.Container.TRANSITION_COMPLETE,this.hideList),this.unorderedList.to({duration:this.dropConfig.durationContract,toVars:{opacity:"0"}})},i.prototype.hideList=function(){this.unorderedList.removeEventListener(t.Container.TRANSITION_COMPLETE,this.hideList),this.unorderedList.style({display:"none"})},i.prototype.disable=function(){this.button.disable(),this.button.removeEventListener(t.Button.CLICK,this.buttonClicked)},i.prototype.enable=function(){this.button.enable(),this.button.addEventListener(this,t.Button.CLICK,this.buttonClicked)},i.CHANGE="change",i}(t.Container);t.DropDown=n}(t||(t={}));var n="object"==typeof global&&global&&global.Object===Object&&global,i="object"==typeof self&&self&&self.Object===Object&&self,o=n||i||Function("return this")(),r="object"==typeof exports&&exports&&!exports.nodeType&&exports,s=r&&"object"==typeof module&&module&&!module.nodeType&&module,a=s&&s.exports===r;a&&n.process;"function"==typeof define&&"object"==typeof define.amd&&define.amd?(o.shiva=t,define(function(){return t})):s?((s.exports=t).shiva=t,r.shiva=t):o.shiva=t}.call(this);
}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{}],2:[function(require,module,exports){
"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var shiva_1 = require('shiva');
var Header_1 = require('./components/Header');
var Faq_1 = require('./pages/Faq');
var Home_1 = require('./pages/Home');
var Model_1 = require('./Model');
var App = (function (_super) {
    __extends(App, _super);
    function App() {
        _super.call(this, {
            root: true,
            height: "",
        });
        this.main();
    }
    App.prototype.main = function () {
        var _this = this;
        var header = new Header_1.Header();
        this.addChild(header);
        this.pages = new shiva_1.Pages({
            delayTransition: 1,
            pages: {
                "": Home_1.Home,
                "products": Faq_1.Faq,
                "support": Faq_1.Faq,
                "about": Faq_1.Faq,
                "faq": Faq_1.Faq,
                "contact": Faq_1.Faq
            },
            id: "pages"
        });
        this.addChild(this.pages);
        shiva_1.Observer.addEventListener(this, Model_1.Model.NAV_CLICKED, function (e) { _this.updateLocation(e); });
        window.addEventListener('popstate', function (event) {
            var path;
            if (event.state === null) {
                path = window.location.pathname;
                path = path.replace("/", "");
            }
            else {
                path = event.state;
            }
            _this.pages.update(decodeURIComponent(path));
        });
        var path = window.location.pathname;
        path = path.replace(/^\//g, "");
        this.pages.update(decodeURIComponent(path));
    };
    App.prototype.updateLocation = function (e) {
        var path = e.data;
        if (path === "") {
            path = "/";
        }
        history.pushState(null, null, path);
        this.pages.update(decodeURIComponent(e.data));
    };
    return App;
}(shiva_1.Container));
window.onload = function () {
    var begin = new App();
};
},{"./Model":3,"./components/Header":6,"./pages/Faq":10,"./pages/Home":11,"shiva":1}],3:[function(require,module,exports){
"use strict";
var Model = (function () {
    function Model() {
    }
    Model.NAV_CLICKED = "NAV_CLICKED";
    Model.pages = [
        {
            name: "",
            nav: false,
            heroImage: "/images/P3200754.jpg",
            callouts: []
        },
        {
            name: "products",
            heroImage: "/images/DSC_0724-2.jpg",
            callouts: [
                {
                    title: "ibwlerkkj",
                    desc: "asldfhl alksdjfhadfdba askdhfnodufhgsj bfbdfuhg isudfuhg oisdfbgoisbdifb i bdfg i idf i dfgoi sg dfg hidfhgio sdfgboiuh nkugyuf."
                },
                {
                    title: "boihgi",
                    desc: "swlgihn fvouh obiv ifudh bfi ufv isuuf isudv sidjdv isudbv isuduh isudv isuddv isuduv isuddhv sudh isuhsidvn of v."
                }
            ]
        },
        {
            name: "support",
            heroImage: "/images/P3200754.jpg",
            callouts: [
                {
                    title: "aksdjfh",
                    desc: "asldfhl alksdjfhadfdba askdhfnodufhgsj bfbdfuhg isudfuhg oisdfbgoisbdifb i bdfg i idf i dfgoi sg dfg hidfhgio sdfgboiuh nkugyuf."
                },
                {
                    title: "aksdjfh srf",
                    desc: "swlgihn fvouh obiv ifudh bfi ufv isuuf isudv sidjdv isudbv isuduh isudv isuddv isuduv isuddhv sudh isuhsidvn of v."
                }
            ]
        },
        {
            name: "about",
            heroImage: "/images/P3250883.jpg",
            callouts: [
                {
                    title: "fsdf",
                    desc: "asldfhl alksdjfhadfdba askdhfnodufhgsj bfbdfuhg isudfuhg oisdfbgoisbdifb i bdfg i idf i dfgoi sg dfg hidfhgio sdfgboiuh nkugyuf."
                },
                {
                    title: "fsdf luhkb",
                    desc: "swlgihn fvouh obiv ifudh bfi ufv isuuf isudv sidjdv isudbv isuduh isudv isuddv isuduv isuddhv sudh isuhsidvn of v."
                }
            ]
        },
        {
            name: "faq",
            heroImage: "/images/P3250904.jpg",
            callouts: [
                {
                    title: "moierv",
                    desc: "asldfhl alksdjfhadfdba askdhfnodufhgsj bfbdfuhg isudfuhg oisdfbgoisbdifb i bdfg i idf i dfgoi sg dfg hidfhgio sdfgboiuh nkugyuf."
                },
                {
                    title: "moierv sglknhse",
                    desc: "swlgihn fvouh obiv ifudh bfi ufv isuuf isudv sidjdv isudbv isuduh isudv isuddv isuduv isuddhv sudh isuhsidvn of v."
                }
            ]
        },
        {
            name: "contact",
            heroImage: "/images/P4110634.jpg",
            callouts: [
                {
                    title: "ksudgf",
                    desc: "asldfhl alksdjfhadfdba askdhfnodufhgsj bfbdfuhg isudfuhg oisdfbgoisbdifb i bdfg i idf i dfgoi sg dfg hidfhgio sdfgboiuh nkugyuf."
                },
                {
                    title: "ksudgf sdfbw",
                    desc: "swlgihn fvouh obiv ifudh bfi ufv isuuf isudv sidjdv isudbv isuduh isudv isuddv isuduv isuddhv sudh isuhsidvn of v."
                }
            ]
        }
    ];
    return Model;
}());
exports.Model = Model;
},{}],4:[function(require,module,exports){
"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var shiva_1 = require('shiva');
var Style_1 = require('./../styles/Style');
var Callout = (function (_super) {
    __extends(Callout, _super);
    function Callout(_a) {
        var _this = this;
        var titleText = _a.title, descText = _a.desc;
        _super.call(this, {
            id: "callout",
        });
        var title = new shiva_1.Container({
            text: titleText,
            style: Style_1.Style.h2,
        });
        this.addChild(title);
        var desc = new shiva_1.Container({
            text: descText,
            style: Style_1.Style.body,
        });
        this.addChild(desc);
        window.addEventListener("resize", function (e) { _this.layout(e); });
        this.layout(null);
    }
    Callout.prototype.wide = function () {
    };
    Callout.prototype.narrow = function () {
    };
    Callout.prototype.layout = function (e) {
        if (shiva_1.Window.width > 700) {
            this.wide();
        }
        else {
            this.narrow();
        }
    };
    return Callout;
}(shiva_1.Container));
exports.Callout = Callout;
},{"./../styles/Style":12,"shiva":1}],5:[function(require,module,exports){
"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var shiva_1 = require('shiva');
var Style_1 = require('./../styles/Style');
var Model_1 = require('./../Model');
var Callout_1 = require('./Callout');
var Hero_1 = require('./Hero');
var CalloutHero = (function (_super) {
    __extends(CalloutHero, _super);
    function CalloutHero(id) {
        var _this = this;
        _super.call(this, {
            id: id,
        });
        this._id = id;
        this.slide = new shiva_1.Container({
            // width: "100%",
            // left: "100%",
            // position: "relative",
            transform: "translateX(100%)",
            opacity: "1",
            backgroundColor: Style_1.Style.colours.verydarkgrey,
            id: "slide"
        });
        this.addChild(this.slide);
        var calloutData = Model_1.Model.pages.filter(function (item) {
            return item.name === _this._id;
        })[0].callouts;
        // console.log(callout);
        // callout = callout;
        var calloutsContainer = new shiva_1.Container({
            paddingTop: "5rem"
        });
        this.slide.addChild(calloutsContainer);
        this.callouts = [];
        calloutData.map(function (item) {
            var callout = new Callout_1.Callout(item);
            calloutsContainer.addChild(callout);
            _this.callouts.push(callout);
        });
        var heroImage = Model_1.Model.pages.filter(function (item) {
            return item.name === _this._id;
        })[0].heroImage;
        this.hero = new Hero_1.Hero({
            image: heroImage
        });
        this.slide.addChild(this.hero);
        window.addEventListener("resize", function (e) { _this.layout(e); });
        this.layout(null);
    }
    CalloutHero.prototype.wide = function () {
        this.callouts.map(function (callout) {
            callout.style({
                paddingBottom: "5rem",
                width: "calc(50% - 6rem)",
                verticalAlign: "top",
                display: "inline-block"
            });
        });
    };
    CalloutHero.prototype.narrow = function () {
        this.callouts.map(function (callout) {
            callout.style({
                paddingBottom: "5rem",
                width: "calc(100% - 6rem)",
                verticalAlign: "top",
                display: "block"
            });
        });
    };
    CalloutHero.prototype.layout = function (e) {
        if (shiva_1.Window.width > Style_1.Style.breakpoint) {
            this.wide();
        }
        else {
            this.narrow();
        }
    };
    CalloutHero.prototype.wake = function () {
        this.to({
            toVars: {
                opacity: "1"
            },
            duration: 1
        });
        this.slide.fromTo({
            immediateRender: true,
            fromVars: {
                // left: "100%"
                transform: "translateX(100%)",
            },
            toVars: {
                transform: "translateX(0%)",
            },
            duration: 0.7,
            ease: shiva_1.Ease.EaseOut
        });
        var delay = 0.3;
        this.callouts.map(function (callout) {
            callout.fromTo({
                immediateRender: true,
                duration: 2,
                delay: delay,
                fromVars: {
                    transform: "translateX(6rem)",
                    opacity: "0"
                },
                toVars: {
                    transform: "translateX(3rem)",
                    opacity: "1"
                }
            });
            delay = delay * 3;
        });
    };
    CalloutHero.prototype.sleep = function () {
        this.to({
            toVars: {
                opacity: "0"
            },
            duration: 1
        });
    };
    return CalloutHero;
}(shiva_1.Container));
exports.CalloutHero = CalloutHero;
},{"./../Model":3,"./../styles/Style":12,"./Callout":4,"./Hero":7,"shiva":1}],6:[function(require,module,exports){
"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var shiva_1 = require('shiva');
var Style_1 = require('./../styles/Style');
var NavItem_1 = require('./NavItem');
var Model_1 = require('./../Model');
var Header = (function (_super) {
    __extends(Header, _super);
    function Header() {
        _super.call(this, {
            id: "header",
        });
        var bg = new shiva_1.Container({
            backgroundColor: Style_1.Style.colours.verydarkgrey,
            width: "100%",
            display: "inline-block",
        });
        this.addChild(bg);
        bg.fromTo({
            duration: 2,
            fromVars: {
                opacity: "0"
            },
            toVars: {
                opacity: "1"
            }
        });
        var title = new NavItem_1.NavItem("shiva", Style_1.Style.navHome, "/");
        title.style({
            marginTop: "10rem",
            marginLeft: Style_1.Style.marginLeft,
        });
        title.addEventListener(this, "click", this.titleClicked);
        title.fromTo({
            duration: 1,
            fromVars: {
                marginTop: "8rem"
            },
            toVars: {
                marginTop: "10rem"
            }
        });
        bg.addChild(title);
        var rule = new shiva_1.Container(Style_1.Style.rule);
        bg.addChild(rule);
        rule.fromTo({
            duration: 1,
            fromVars: {
                marginTop: "3rem"
            },
            toVars: {
                marginTop: "0rem"
            }
        });
    }
    Header.prototype.titleClicked = function (e) {
        this.preventDefault(e.sourceEvent);
        shiva_1.Observer.dispatchEvent(new shiva_1.Event(Model_1.Model.NAV_CLICKED, this, ""));
    };
    return Header;
}(shiva_1.Container));
exports.Header = Header;
},{"./../Model":3,"./../styles/Style":12,"./NavItem":9,"shiva":1}],7:[function(require,module,exports){
"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var shiva_1 = require('shiva');
var Hero = (function (_super) {
    __extends(Hero, _super);
    function Hero(_a) {
        var image = _a.image;
        _super.call(this, {
            id: "hero-image",
        });
        this.imageContainer = new shiva_1.Image({
            path: image,
            style: {
                width: "100%",
                opacity: "0"
            }
        });
        this.imageContainer.addEventListener(this, "load", this.imageLoaded);
        this.addChild(this.imageContainer);
    }
    Hero.prototype.imageLoaded = function (e) {
        this.fadeUpImage();
    };
    Hero.prototype.fadeUpImage = function () {
        this.imageContainer.fromTo({
            duration: 5,
            fromVars: {
                opacity: "0"
            },
            toVars: {
                opacity: "1"
            }
        });
    };
    return Hero;
}(shiva_1.Container));
exports.Hero = Hero;
},{"shiva":1}],8:[function(require,module,exports){
"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var shiva_1 = require('shiva');
var Style_1 = require('./../styles/Style');
var NavItem_1 = require('./NavItem');
var Model_1 = require('./../Model');
var Nav = (function (_super) {
    __extends(Nav, _super);
    function Nav(items) {
        var _this = this;
        _super.call(this, {
            id: "nav",
        });
        this.itemsArr = [];
        var count = 1;
        items.map(function (item) {
            var itemContainer = new NavItem_1.NavItem(item, Style_1.Style.nav, item);
            itemContainer.style({
                marginBottom: "0.5rem"
            });
            itemContainer.addEventListener(_this, "click", _this.itemClicked, item, false);
            _this.addChild(itemContainer);
            _this.itemsArr.push(itemContainer);
        });
        this.animate();
    }
    Nav.prototype.animate = function () {
        var count = 1;
        this.itemsArr.map(function (item) {
            item.fromTo({
                immediateRender: true,
                delay: count,
                duration: 1,
                fromVars: {
                    opacity: "0",
                    transform: "translateX(1rem)"
                },
                toVars: {
                    transform: "translateX(0rem)",
                    opacity: "1"
                },
                ease: shiva_1.Ease.EaseOut
            });
            count = count * 1.3;
        });
    };
    Nav.prototype.itemClicked = function (e) {
        var sourceEvent = e.sourceEvent;
        sourceEvent.preventDefault();
        var item = e.target;
        item.click(null);
        shiva_1.Observer.dispatchEvent(new shiva_1.Event(Model_1.Model.NAV_CLICKED, this, e.data));
    };
    return Nav;
}(shiva_1.Container));
exports.Nav = Nav;
},{"./../Model":3,"./../styles/Style":12,"./NavItem":9,"shiva":1}],9:[function(require,module,exports){
"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var shiva_1 = require('shiva');
var NavItem = (function (_super) {
    __extends(NavItem, _super);
    function NavItem(labelText, style, href) {
        _super.call(this, {
            id: "nav-" + labelText,
        });
        this.backgroundOut = style.backgroundOut;
        this.backgroundOver = style.backgroundOver;
        this.label = new shiva_1.Anchor({
            text: labelText,
            style: style,
            href: href,
            display: "block"
        });
        this.addChild(this.label);
        this.addEventListener(this, "mouseover", this.over);
        this.addEventListener(this, "mouseout", this.out);
        this.style({
            cursor: "pointer",
        });
    }
    NavItem.prototype.over = function (e) {
        this.label.to({
            duration: 0.1,
            toVars: {
                backgroundColor: this.backgroundOver
            }
        });
    };
    NavItem.prototype.out = function (e) {
        this.label.to({
            duration: 2,
            toVars: {
                backgroundColor: this.backgroundOut
            }
        });
    };
    NavItem.prototype.click = function (e) {
        this.label.fromTo({
            duration: 2,
            fromVars: {
                backgroundColor: this.backgroundOver
            },
            toVars: {
                backgroundColor: this.backgroundOut
            }
        });
    };
    return NavItem;
}(shiva_1.Container));
exports.NavItem = NavItem;
},{"shiva":1}],10:[function(require,module,exports){
"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var shiva_1 = require('shiva');
var CalloutHero_1 = require('./../components/CalloutHero');
var Faq = (function (_super) {
    __extends(Faq, _super);
    function Faq(id) {
        _super.call(this, {
            id: id,
        });
        this.calloutHero = new CalloutHero_1.CalloutHero(id);
        this.addChild(this.calloutHero);
    }
    Faq.prototype.sleep = function () {
        this.calloutHero.sleep();
    };
    Faq.prototype.wake = function () {
        this.calloutHero.wake();
    };
    return Faq;
}(shiva_1.Container));
exports.Faq = Faq;
},{"./../components/CalloutHero":5,"shiva":1}],11:[function(require,module,exports){
"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var shiva_1 = require('shiva');
var Style_1 = require('./../styles/Style');
var Nav_1 = require('./../components/Nav');
var Model_1 = require('./../Model');
var Hero_1 = require('./../components/Hero');
var Home = (function (_super) {
    __extends(Home, _super);
    function Home(id) {
        _super.call(this, {
            id: id,
        });
        var bg = new shiva_1.Container({
            backgroundColor: Style_1.Style.colours.verydarkgrey,
            width: "100%",
            paddingTop: "3rem",
        });
        this.addChild(bg);
        bg.fromTo({
            duration: 2,
            fromVars: {
                opacity: "0"
            },
            toVars: {
                opacity: "1"
            }
        });
        var navItems = Model_1.Model.pages.filter(function (item) {
            return item.nav === undefined;
        }).map(function (item) {
            return item.name;
        });
        this.nav = new Nav_1.Nav(navItems);
        this.nav.style({
            marginLeft: "3rem",
            paddingBottom: "3rem"
        });
        bg.addChild(this.nav);
        var heroImage = Model_1.Model.pages.filter(function (item) {
            return item.name === id;
        })[0].heroImage;
        this.hero = new Hero_1.Hero({
            image: heroImage
        });
        this.addChild(this.hero);
        this.hero.alpha = 0;
        this.hero.to({
            duration: 2,
            delay: 2,
            toVars: {
                opacity: "1"
            }
        });
    }
    Home.prototype.sleep = function () {
        this.to({
            toVars: {
                opacity: "0"
            },
            duration: 1
        });
    };
    Home.prototype.wake = function () {
        this.fromTo({
            fromVars: {
                transform: "translateX(100%)",
                opacity: "0"
            },
            toVars: {
                transform: "translateX(0%)",
                opacity: "1"
            },
            duration: 1,
            ease: shiva_1.Ease.EaseOut
        });
        this.nav.animate();
    };
    return Home;
}(shiva_1.Container));
exports.Home = Home;
},{"./../Model":3,"./../components/Hero":7,"./../components/Nav":8,"./../styles/Style":12,"shiva":1}],12:[function(require,module,exports){
"use strict";
var Style = (function () {
    function Style() {
    }
    Style.colours = {
        purple: "#333366",
        transparentPurple: "rgba(51, 51, 102, 0.5)",
        lightpurple: "#9999cc",
        blue: "#4444ee",
        lightgrey: "#eeeeee",
        green: "#229922",
        darkgrey: "#555555",
        wine: "#440000",
        verydarkgrey: "#333333"
    };
    Style.fonts = {
        standardFont: "Bree Serif"
    };
    Style.breakpoint = 800;
    Style.marginLeft = "3rem";
    Style.h2 = {
        fontSize: "1rem",
        color: Style.colours.lightpurple,
        fontFamily: Style.fonts.standardFont,
        lineHeight: "1.3rem"
    };
    Style.nav = {
        fontSize: "1rem",
        color: Style.colours.lightpurple,
        fontFamily: Style.fonts.standardFont,
        backgroundOver: "rgba(75, 75, 75, 1)",
        backgroundOut: "rgba(255, 255, 255, 0)"
    };
    Style.navHome = {
        fontSize: "1.8rem",
        color: Style.colours.lightpurple,
        fontFamily: Style.fonts.standardFont,
        backgroundOver: "rgba(75, 75, 75, 1)",
        backgroundOut: "rgba(255, 255, 255, 0)"
    };
    Style.body = {
        fontSize: "1rem",
        color: "#cccccc",
        fontFamily: Style.fonts.standardFont,
        lineHeight: "1.3rem",
    };
    Style.rule = {
        height: "1px",
        opacity: "0.2",
        backgroundColor: Style.colours.lightpurple,
        width: "100%",
    };
    return Style;
}());
exports.Style = Style;
},{}]},{},[2])


//# sourceMappingURL=bundle.js.map

# shiva pages #
---
### Example application built with shiva :trident:  ###

All behaviour and presentation in this example application is achieved with code; no markup or CSS. Uses Typescript with a Gulp and Browserify build system.  

The focus of this example is to demonstrate the `Pages` helper class to provide simple routing behaviour.

---
### Install

```
git clone "https://bitbucket.org/gabrielmccallin/shiva-pages.git"
npm install
``` 

---
### Run
```
npm start
```
or
```
gulp
``` 

This will launch a web server at `http://localhost:1337` displaying the application.

---
### Usage
```
this.pages = new Pages({
    delayTransition: 1,
    pages: {
        "": Home,
        "products": Products,
        "support": Support,
        "about": About,
        "faq": Faq,
        "contact": Contact
    }
});

this.addChild(this.pages);

// and then change page when necessary 
this.pages.update("faq");
```  

`Pages` can manage a set of classes, showing (instantiating if necessary) a requested class and hiding the current class. This provides a method of routing within the application.       

This example also makes use of the `History` API to connect `Pages` to the address bar.  

The web server is configured to fallback to `index.html` whenever a URL that doesn't exist is requested. This allows the application to respond to all requests and show the appropriate section. Try it out! :grin:


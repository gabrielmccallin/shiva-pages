﻿export class Style {
    static colours = {
        purple: "#333366",
        transparentPurple: "rgba(51, 51, 102, 0.5)",
        lightpurple: "#9999cc",
        blue: "#4444ee",
        lightgrey: "#eeeeee",
        green: "#229922",
        darkgrey: "#555555",
        wine: "#440000",
        verydarkgrey: "#333333"
    };

    static fonts = {
        standardFont: "Bree Serif"
    };

    static breakpoint = 800;
    static marginLeft = "3rem";

    static h2 = {
        fontSize: "1rem",
        color: Style.colours.lightpurple,
        fontFamily: Style.fonts.standardFont,
        lineHeight: "1.3rem"
    };

    static nav = {
        fontSize: "1rem",
        color: Style.colours.lightpurple,
        fontFamily: Style.fonts.standardFont,
        backgroundOver: "rgba(75, 75, 75, 1)",
        backgroundOut: "rgba(255, 255, 255, 0)"
    };

    static navHome = {
        fontSize: "1.8rem",
        color: Style.colours.lightpurple,
        fontFamily: Style.fonts.standardFont,
        backgroundOver: "rgba(75, 75, 75, 1)",
        backgroundOut: "rgba(255, 255, 255, 0)"
    };

    static body = {
        fontSize: "1rem",
        color: "#cccccc",
        fontFamily: Style.fonts.standardFont,
        lineHeight: "1.3rem",
        // letterSpacing: "-0.07rem"
    };

    static rule = {
        height: "1px",
        opacity: "0.2",
        backgroundColor: Style.colours.lightpurple,
        width: "100%",
    };


}
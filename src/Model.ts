﻿export class Model {

    static NAV_CLICKED = "NAV_CLICKED";

    static pages = [
        {
            name: "",
            nav: false,
            heroImage: "/images/P3200754.jpg",
            callouts: []
        },
        {
            name: "products",
            heroImage: "/images/DSC_0724-2.jpg",
            callouts: [
                {
                    title: "ibwlerkkj",
                    desc: "asldfhl alksdjfhadfdba askdhfnodufhgsj bfbdfuhg isudfuhg oisdfbgoisbdifb i bdfg i idf i dfgoi sg dfg hidfhgio sdfgboiuh nkugyuf."
                },
                {
                    title: "boihgi",
                    desc: "swlgihn fvouh obiv ifudh bfi ufv isuuf isudv sidjdv isudbv isuduh isudv isuddv isuduv isuddhv sudh isuhsidvn of v."
                }
            ]
        },
        {
            name: "support",
            heroImage: "/images/P3200754.jpg",
            callouts: [
                {
                    title: "aksdjfh",
                    desc: "asldfhl alksdjfhadfdba askdhfnodufhgsj bfbdfuhg isudfuhg oisdfbgoisbdifb i bdfg i idf i dfgoi sg dfg hidfhgio sdfgboiuh nkugyuf."
                },
                {
                    title: "aksdjfh srf",
                    desc: "swlgihn fvouh obiv ifudh bfi ufv isuuf isudv sidjdv isudbv isuduh isudv isuddv isuduv isuddhv sudh isuhsidvn of v."
                }
            ]
        },
        {
            name: "about",
            heroImage: "/images/P3250883.jpg",
            callouts: [
                {
                    title: "fsdf",
                    desc: "asldfhl alksdjfhadfdba askdhfnodufhgsj bfbdfuhg isudfuhg oisdfbgoisbdifb i bdfg i idf i dfgoi sg dfg hidfhgio sdfgboiuh nkugyuf."
                },
                {
                    title: "fsdf luhkb",
                    desc: "swlgihn fvouh obiv ifudh bfi ufv isuuf isudv sidjdv isudbv isuduh isudv isuddv isuduv isuddhv sudh isuhsidvn of v."
                }
            ]
        },
        {
            name: "faq",
            heroImage: "/images/P3250904.jpg",
            callouts: [
                {
                    title: "moierv",
                    desc: "asldfhl alksdjfhadfdba askdhfnodufhgsj bfbdfuhg isudfuhg oisdfbgoisbdifb i bdfg i idf i dfgoi sg dfg hidfhgio sdfgboiuh nkugyuf."
                },
                {
                    title: "moierv sglknhse",
                    desc: "swlgihn fvouh obiv ifudh bfi ufv isuuf isudv sidjdv isudbv isuduh isudv isuddv isuduv isuddhv sudh isuhsidvn of v."
                }
            ]
        },
        {
            name: "contact",
            heroImage: "/images/P4110634.jpg",
            callouts: [
                {
                    title: "ksudgf",
                    desc: "asldfhl alksdjfhadfdba askdhfnodufhgsj bfbdfuhg isudfuhg oisdfbgoisbdifb i bdfg i idf i dfgoi sg dfg hidfhgio sdfgboiuh nkugyuf."
                },
                {
                    title: "ksudgf sdfbw",
                    desc: "swlgihn fvouh obiv ifudh bfi ufv isuuf isudv sidjdv isudbv isuduh isudv isuddv isuduv isuddhv sudh isuhsidvn of v."
                }
            ]
        }
    ]

}

import { Container, Window, Ease, Image, Observer, Event } from 'shiva';

import { Style } from './../styles/Style';
import { Nav } from './../components/Nav';
import { Model } from './../Model';
import { Hero } from './../components/Hero';
import { CalloutHero } from './../components/CalloutHero';

export class Contact extends Container {
    private calloutHero: CalloutHero;

    constructor(id:string) {
        super({
            id: id,
        });

        this.calloutHero = new CalloutHero(id);
        this.addChild(this.calloutHero);

    }



    private sleep() {
        this.calloutHero.sleep();
    }

    private wake() {
        this.calloutHero.wake();
    }

}
import { Container, Window, Ease, Image, Observer, Event } from 'shiva';

import { Style } from './../styles/Style';
import { Nav } from './../components/Nav';
import { Model } from './../Model';
import { Hero } from './../components/Hero';

export class Home extends Container {
    private hero: Hero;
    private nav: Nav;

    constructor(id: string) {
        super({
            id: id,
        });

        let bg = new Container({
            backgroundColor: Style.colours.verydarkgrey,
            width: "100%",
            paddingTop: "3rem",
        });
        this.addChild(bg);
        bg.fromTo({
            duration: 2,
            fromVars: {
                opacity: "0"
            },
            toVars: {
                opacity: "1"
            }
        });

        let navItems = Model.pages.filter((item: any) => {
            return item.nav === undefined;
        }).map((item) => {
            return item.name;
        });

        this.nav = new Nav(navItems);
        this.nav.style({
            marginLeft: "3rem",
            paddingBottom: "3rem"
        })
        bg.addChild(this.nav);

        let heroImage = Model.pages.filter((item: any) => {
            return item.name === id;
        })[0].heroImage;

        this.hero = new Hero({
            image: heroImage
        });
        this.addChild(this.hero);
        this.hero.alpha = 0;
        this.hero.to({
            duration: 2,
            delay: 2,
            toVars: {
                opacity: "1"
            }
        });

    }

    private sleep() {
        this.to({
            toVars: {
                opacity: "0"
            },
            duration: 1
        });
    }

    private wake() {
        this.fromTo({
            fromVars: {
                transform: "translateX(100%)",
                opacity: "0"
            },
            toVars: {
                transform: "translateX(0%)",
                opacity: "1"
            },
            duration: 1,
            ease: Ease.EaseOut
        });
        this.nav.animate();
    }
}
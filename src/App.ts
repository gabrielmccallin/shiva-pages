﻿import { Container, Pages, Observer, Event } from 'shiva';

import { Style } from './styles/Style';
import { Header } from './components/Header';
import { About } from './pages/About';
import { Contact } from './pages/Contact';
import { Faq } from './pages/Faq';
import { Home } from './pages/Home';
import { Products } from './pages/Products';
import { Support } from './pages/Support';
import { Model } from './Model';


class App extends Container {

    private pages: Pages;

    constructor() {

        super({
            root: true,
            height: "",
        });

        this.main();
    }

    private main() {

        let header = new Header();
        this.addChild(header);



        this.pages = new Pages({
            delayTransition: 1,
            pages: {
                "": Home,
                "products": Faq,
                "support": Faq,
                "about": Faq,
                "faq": Faq,
                "contact": Faq
            },
            id: "pages"
        });
        this.addChild(this.pages);

        Observer.addEventListener(this, Model.NAV_CLICKED, (e: Event) => { this.updateLocation(e); });

        window.addEventListener('popstate', (event) => {
            let path: string;
            if (event.state === null) {
                path = window.location.pathname;
                path = path.replace("/", "");
            }
            else {
                path = event.state;
            }
            this.pages.update(decodeURIComponent(path));

        });

        let path = window.location.pathname;
        path = path.replace(/^\//g, "");

        this.pages.update(decodeURIComponent(path));
    }


    private updateLocation(e: Event) {
        let path = e.data;
        if (path === "") {
            path = "/";
        }

        history.pushState(null, null, path);

        this.pages.update(decodeURIComponent(e.data));
    }



}

window.onload = () => {
    let begin = new App();
};
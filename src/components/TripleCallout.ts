import { Container, Window } from 'shiva';
import { Callout } from './Callout'; 
import { Style } from './../styles/Style'; 


export class TripleCallout extends Container {

    private wideCalloutWidth = "33.33333%";
    private narrowCalloutWidth = "100%";
    private callouts = [];

    constructor(content: any[]) {
        super({
            id: "callout-container",
        });

        let bg = new Container({
            backgroundColor: Style.colours.verydarkgrey,
            paddingTop: "2rem",
            paddingBottom: "2rem",
            paddingLeft: "1.5rem",
            paddingRight: "1.5rem",
            opacity: "0"

        });
        this.addChild(bg);
        bg.to({
            duration: 7,
            toVars:{
                opacity: "1"
            }
        });



        let count = 1;
        content.map((vo) => {
            let callout = new Callout({
                title: vo.title,
                desc: vo.desc
            });
            bg.addChild(callout);
            callout.style({
                opacity: "0"
            });

            callout.fromTo({
                duration: 1,
                delay: count,
                fromVars: { opacity: "0", top: "30px" }, 
                toVars: { opacity: "1", top: "0px" }
            });
            count = count * 1.5;

            this.callouts.push(callout);
        });

        window.addEventListener("resize", (e) => { this.layout(e) });

        this.layout(null);
    }

    private wide() {
        let calloutWidth = this.wideCalloutWidth;
        this.callouts.map((callout: Callout) => {
            callout.style({
                display: "inline-block",
                width: calloutWidth,
                verticalAlign: "top",
                position: "relative"
            });
        });
    }

    private narrow() {
        let calloutWidth = this.narrowCalloutWidth;
        this.callouts.map((callout) => {
            callout.style({
                width: calloutWidth,
                marginTop: "1rem",
                marginBottom: "2rem",
                display: "block"
            });
        });
    }

    private layout(e: Event) {
        if (Window.width > Style.breakpoint) {
            this.wide();
        }
        else {
            this.narrow();
        }
    }
}
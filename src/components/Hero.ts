import { Container, Window, Ease, Image, Event } from 'shiva';
import { Style } from './../styles/Style';

export class Hero extends Container {

    private imageContainer: Image;

    constructor({
        image: image
    }) {
        super({
            id: "hero-image",
        });

        this.imageContainer = new Image({
            path: image,
            style: {
                width: "100%",
                opacity: "0"
            }
        });
        this.imageContainer.addEventListener(this, "load", this.imageLoaded);
        this.addChild(this.imageContainer);

    }

    private imageLoaded(e: Event) {
        this.fadeUpImage();
    }

    fadeUpImage() {
        this.imageContainer.fromTo({
            duration: 5,
            fromVars: {
                opacity: "0"
            },
            toVars: {
                opacity: "1"
            }
        });
    }

}
import { Container, Window } from 'shiva';
import { Style } from './../styles/Style';


export class Callout extends Container {

    private curlyTitle: Container;

    constructor({title: titleText, desc: descText}) {
        super({
            id: "callout",
        });

        let title = new Container({
            text: titleText,
            style: Style.h2,
        });
        this.addChild(title);

        let desc = new Container({
            text: descText,
            style: Style.body,
        });
        this.addChild(desc);


        window.addEventListener("resize", (e) => { this.layout(e) });

        this.layout(null);
    }

    private wide() {

    }

    private narrow() {

    }

    private layout(e: Event) {
        if (Window.width > 700) {
            this.wide();
        }
        else {
            this.narrow();
        }
    }
}
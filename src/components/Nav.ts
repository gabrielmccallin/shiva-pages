import { Container, Window, Ease, Image, Event, Observer } from 'shiva';
import { Style } from './../styles/Style';
import { NavItem } from './NavItem';
import { Model } from './../Model';

export class Nav extends Container {
    private itemsArr: Container[] = [];

    constructor(items) {
        super({
            id: "nav",
        });

        let count = 1;
        items.map((item) => {

            let itemContainer = new NavItem(item, Style.nav, item);
            itemContainer.style({
                marginBottom: "0.5rem"
            });

            itemContainer.addEventListener(this, "click", this.itemClicked, item, false);
            this.addChild(itemContainer);
            this.itemsArr.push(itemContainer);
        });
        this.animate();

    }

    animate() {
        let count = 1;
        this.itemsArr.map((item) => {
            item.fromTo({
                immediateRender: true,
                delay: count,
                duration: 1,
                fromVars: {
                    opacity: "0",
                    transform: "translateX(1rem)"
                },
                toVars: {
                    transform: "translateX(0rem)",
                    opacity: "1"
                },
                ease: Ease.EaseOut
            });
            count = count * 1.3;
        });
    }

    private itemClicked(e: Event) {
        let sourceEvent = <UIEvent>e.sourceEvent;
        sourceEvent.preventDefault();

        let item = <NavItem>e.target;
        item.click(null);
        Observer.dispatchEvent(new Event(Model.NAV_CLICKED, this, e.data));
    }

}
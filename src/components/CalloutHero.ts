import { Container, Window, Ease, Image } from 'shiva';
import { Style } from './../styles/Style';
import { Nav } from './Nav';
import { Model } from './../Model';
import { Callout } from './Callout';
import { Hero } from './Hero';

export class CalloutHero extends Container {

    private slide: Container;
    private hero: Hero;
    private callouts: Container[];
    private _id;

    constructor(id: string) {
        super({
            id: id,
        });
        this._id = id;

        this.slide = new Container({
            // width: "100%",
            // left: "100%",
            // position: "relative",
            transform: "translateX(100%)",
            opacity: "1",
            backgroundColor: Style.colours.verydarkgrey,
            id: "slide"
        });
        this.addChild(this.slide);

        let calloutData = <Array<{}>>Model.pages.filter((item: any) => {
            return item.name === this._id;
        })[0].callouts;

        // console.log(callout);
        // callout = callout;
        let calloutsContainer = new Container({
            paddingTop: "5rem"
        });
        this.slide.addChild(calloutsContainer);


        this.callouts = [];

        calloutData.map((item: { title: string, desc: string }) => {

            let callout = new Callout(item);
            calloutsContainer.addChild(callout);
            this.callouts.push(callout);
        })

        let heroImage = Model.pages.filter((item: any) => {
            return item.name === this._id;
        })[0].heroImage;

        this.hero = new Hero({
            image: heroImage
        });
        this.slide.addChild(this.hero);

        window.addEventListener("resize", (e) => { this.layout(e) });

        this.layout(null);
    }

    private wide() {
        this.callouts.map((callout) => {
            callout.style({
                paddingBottom: "5rem",
                width: "calc(50% - 6rem)",
                verticalAlign: "top",
                display: "inline-block"
            });
        });
    }

    private narrow() {
        this.callouts.map((callout) => {
            callout.style({
                paddingBottom: "5rem",
                width: "calc(100% - 6rem)",
                verticalAlign: "top",
                display: "block"
            });
        });

    }

    private layout(e: UIEvent) {
        if (Window.width > Style.breakpoint) {
            this.wide();
        }
        else {
            this.narrow();
        }


    }

    wake() {
        this.to({
            toVars: {
                opacity: "1"
            },
            duration: 1
        });
        this.slide.fromTo({
            immediateRender: true,
            fromVars: {
                // left: "100%"
                transform: "translateX(100%)",
                // opacity: "0"
            },
            toVars: {
                transform: "translateX(0%)",
                // opacity: "1"
                // left: "0%"
            },
            duration: 0.7,
            ease: Ease.EaseOut
        });

        let delay = 0.3;
        this.callouts.map((callout) => {
            callout.fromTo({
                immediateRender: true,
                duration: 2,
                delay: delay,
                fromVars: {
                    transform: "translateX(6rem)",
                    opacity: "0"
                },
                toVars: {
                    transform: "translateX(3rem)",
                    opacity: "1"
                }
            });
            delay = delay * 3;
        });
    }

    sleep() {
        this.to({
            toVars: {
                opacity: "0"
            },
            duration: 1
        });
    }
}
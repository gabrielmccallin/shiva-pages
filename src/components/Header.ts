import { Container, Window, Ease, Image, Event, Observer } from 'shiva';

import { Style } from './../styles/Style';
import { Nav } from './Nav';
import { NavItem } from './NavItem';
import { Model } from './../Model';

export class Header extends Container {

    constructor() {
        super({
            id: "header",
        });

        let bg = new Container({
            backgroundColor: Style.colours.verydarkgrey,
            width: "100%",
            display: "inline-block",
        });
        this.addChild(bg);
        bg.fromTo({
            duration: 2,
            fromVars: {
                opacity: "0"
            },
            toVars: {
                opacity: "1"
            }
        });

        
        let title = new NavItem("shiva", Style.navHome, "/");
        title.style({
            marginTop: "10rem",
            marginLeft: Style.marginLeft,
        })
        title.addEventListener(this, "click", this.titleClicked);
        title.fromTo({
            duration:1, 
            fromVars: {
                marginTop: "8rem"
            },
            toVars: {
                marginTop: "10rem"
            }
        });
        bg.addChild(title);

        let rule = new Container(Style.rule);
        bg.addChild(rule);
        
        rule.fromTo({
            duration:1, 
            fromVars: {
                marginTop: "3rem"
            },
            toVars: {
                marginTop: "0rem"
            }
        });

    }

    private titleClicked(e: Event) {
        this.preventDefault(e.sourceEvent);
        Observer.dispatchEvent(new Event(Model.NAV_CLICKED, this, ""));
    }
}
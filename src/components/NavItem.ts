import { Container, Window, Ease, Image, Event, Anchor } from 'shiva';
import { Style } from './../styles/Style';

export class NavItem extends Container {
    private bg: Container;
    private label: Container;
    private backgroundOut: string;
    private backgroundOver: string;

    constructor(labelText: string, style: any, href?: string) {
        super({
            id: "nav-" + labelText,
        });

        this.backgroundOut = style.backgroundOut;
        this.backgroundOver = style.backgroundOver;

        this.label = new Anchor({
            text: labelText,
            style: style,
            href: href,
            display: "block"
        });
        this.addChild(this.label);

        this.addEventListener(this, "mouseover", this.over);
        this.addEventListener(this, "mouseout", this.out);

        this.style({
            cursor: "pointer",
        });
    }

    private over(e: Event) {
        this.label.to({
            duration: 0.1,
            toVars: {
                backgroundColor: this.backgroundOver
            }
        })
    }
    private out(e: Event) {
        this.label.to({
            duration: 2,
            toVars: {
                backgroundColor: this.backgroundOut
            }
        })
    }
    click(e: Event) {
        this.label.fromTo({
            duration: 2,
            fromVars: {
                backgroundColor: this.backgroundOver
            },
            toVars: {
                backgroundColor: this.backgroundOut
            }
        });
    }
}